#!/usr/bin/env python3

import os

from yolox.exp import Exp as MyExp

class Exp(MyExp):
    def __init__(self):
        super(Exp, self).__init__()
        self.num_classes = 20
        self.depth = 1.33
        self.width = 1.25
        self.exp_name = "csra"
