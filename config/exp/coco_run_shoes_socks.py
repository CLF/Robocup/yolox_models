#!/usr/bin/env python3

import os

from yolox.exp import Exp as MyExp

class Exp(MyExp):
    def __init__(self):
        super(Exp, self).__init__()
        self.num_classes = 3
        self.depth = 0.33
        self.width = 0.50
        self.exp_name = "coco_shoes_socks"
