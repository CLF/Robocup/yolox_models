import json
import argparse
import os

def make_parser():
    parser = argparse.ArgumentParser("YOLOX train parser")
    parser.add_argument("-n", "--name", type=str, default=None, help="config name", required=True)
    parser.add_argument(
        "-ap",
        "--ann_pth",
        type=str,
        help="annotation path",
        default=None,
        required=True
    )
    parser.add_argument(
        "-e",
        "--exp",
        action='store_false',
        help="do not create exp if flag is set"
    )
    parser.add_argument(
        "-y",
        "--yaml",
        action='store_false',
        help="do not create yaml if flag is set"
    )
    parser.add_argument(
        "-m",
        "--models",
        action='store_false',
        help="do not create models if flag is set",
    )
    parser.add_argument(
        "-pre",
        "--models_prefix",
        type=str,
        help="models prefix",
        default="object/ycb/",
    )
    return parser

def get_num_classes(annotation_path: str):
    with open(annotation_path, "r") as f:
        return len(json.load(f)["categories"])

def create_labels(ann_pth: str, name:  str):
    labels = ""
    with open(ann_pth, "r") as f:
        data = json.load(f)["categories"]
        for x in data:
            labels += f'"{x["id"]}": "{x["name"]}"\n'
    labels_pth = name + "_labels.yaml"
    with open(labels_pth, "w") as f:
        f.write(labels)
    

def create_models(ann_pth: str, name: str, prefix: str):
    models = ""
    with open(ann_pth, "r") as f:
        data = json.load(f)["categories"]
        for x in data:
            models += f'"{x["id"]}": "{prefix}{x["name"]}"\n'
    models_pth = name + "_models.yaml"
    with open(models_pth, "w") as f:
        f.write(models)

def create_exp(ann_pth: str, name: str):
    script = f'''
#!/usr/bin/env python3

from yolox.exp import Exp as MyExp

class Exp(MyExp):
    def __init__(self):
        super(Exp, self).__init__()
        self.num_classes = {get_num_classes(ann_pth)}
        self.depth = 1.33
        self.width = 1.25
        self.exp_name = "{name}"'''

    script_dir = os.path.dirname(__file__)
    exp_pth = f"exp/{name}_exp.py"
    abs_file_path = os.path.join(script_dir, exp_pth)
    
    with open(abs_file_path, "w") as f:
        f.write(script)

    
if __name__ == "__main__":
    args = make_parser().parse_args()
    if args.yaml: create_labels(args.ann_pth, args.name)
    if args.models: create_models(args.ann_pth, args.name, args.models_prefix)
    if args.exp: create_exp(args.ann_pth, args.name)