import os
import random
import cv2

from typing import Tuple, Callable, Union, List, Dict, Optional

from yolox.exp import get_exp
from clf_object_recognition_yolox import recognizer

from label_studio_ml.model import LabelStudioMLBase
from label_studio_ml.utils import get_single_tag_keys, get_local_path
import yaml

LS_URL = "http://localhost:8080"
LS_API_TOKEN = "433d01e8a96c66b7f87c6bee7a29a9b8b3ba3b6f"


# Initialize class inhereted from LabelStudioMLBase
class YoloX(LabelStudioMLBase):
    def __init__(self, project_id: Optional[str] = None, label_config=None, **kwargs):
        # Call base class constructor
        super(YoloX, self).__init__(project_id, label_config)

        # Initialize self variables
        self.from_name, self.to_name, self.value, self.classes = get_single_tag_keys(
            self.parsed_label_config, 'RectangleLabels', 'Image')

        print("INIT: args:")
        for key, value in kwargs.items():
            print("{0} = {1}".format(key, value))
        print(self.classes)

        exp_path = "/vol/tiago/one/nightly/share/yolox_models/config/exp/kassel_exp.py"
        checkpoint = "/vol/tiago/one/nightly/share/yolox_models/models/kassel/best_ckpt.pth"
        labels = "/vol/tiago/one/nightly/share/yolox_models/config/kassel_models.yaml"

        with open(labels) as stream:
            try:
                self.labels = yaml.safe_load(stream)
                self.labels = {int(k):v.split('/')[-1] for k,v in self.labels.items()}
            except yaml.YAMLError as exc:
                print(exc)
        
        self.min_conf = 0.01

        # Load model
        self.exp = get_exp(exp_path,"")
        self.checkpoint = checkpoint
        self.load_model()
        

        # try to build label map from labels.yaml labels to LS labels
        schema = list(self.parsed_label_config.values())[0]
        self.labels_attrs = schema.get("labels_attrs")

    def load_model(self):
        # Load model
        self.recognizer = recognizer.Recognizer(self.checkpoint, self.exp)


    # Function to predict
    def predict(self, tasks, **kwargs):
        """
        Returns the list of predictions based on input list of tasks 
        """

        for task in tasks:

            # Getting URL of the image
            print(task)
            image_url = task['data'][self.value]
            image_path = get_local_path(image_url, task_id=task.get('id'))
            
            image = cv2.imread(image_path)

            # Height and width of image
            original_height, original_width = image.shape[:2]

            print(f"recognize on: {image_path} - {original_width}x{original_height}")

            # Getting prediction using model
            (ids, scores, boxes) = self.recognizer.inference(image, self.min_conf)

            # Creating list for predictions and variable for scores
            predictions = []

            for i in range(len(ids)):
                box = boxes[i]
                cls_id = int(ids[i])
                label = self.labels[cls_id]
                score = float(scores[i])
                x0 = int(box[0])
                y0 = int(box[1])
                x1 = int(box[2])
                y1 = int(box[3])
                thresh = 0.1
                if score < thresh:
                    # tensor is sorted by scores so we should break here
                    continue
                print(f"recognized: '{label}' at: {x0},{y0}-{x1-x0},{y1-y0}")
                predictions.append({
                    "id": str(i),
                    "from_name": self.from_name,
                    "to_name": self.to_name,
                    "type": "rectanglelabels",
                    "score": score,
                    "original_width": original_width,
                    "original_height": original_height,
                    "image_rotation": 0,
                    "value": {
                        "rotation": 0,
                        "x": x0 / original_width * 100,
                        "y": y0 / original_height * 100,
                        "width": (x1-x0) / original_width * 100,
                        "height": (y1-y0) / original_height * 100,
                        "rectanglelabels": [label]
                    }})

            # Dict with final dicts with predictions
            final_prediction = [{
                "result": predictions,
                "score": 1,
                "model_version": "yolox"
            }]

            return final_prediction
    
        

    def fit(self, completions, workdir=None, **kwargs):
        """ 
        Dummy function to train model
        """
        return {'random': random.randint(1, 10)}


    def build_labels_from_labeling_config(self, schema):
        """
        Collect label maps from `predicted_values="airplane,car"` attribute in <Label> tags,
        e.g. "airplane", "car" are label names from COCO dataset
        """
        mmdet_labels = model.dataset_meta.get("classes", [])
        mmdet_labels_lower = [label.lower() for label in mmdet_labels]
        print(
            "COCO dataset labels supported by this mmdet model:",
            model.dataset_meta,
        )

        # if labeling config has Label tags
        if self.labels_attrs:
            # try to find something like <Label value="Vehicle" predicted_values="airplane,car">
            for ls_label, label_attrs in self.labels_attrs.items():
                predicted_values = label_attrs.get("predicted_values", "").split(",")
                for predicted_value in predicted_values:
                    if predicted_value:  # it shouldn't be empty (like '')
                        if predicted_value not in mmdet_labels:
                            print(
                                f'Predicted value "{predicted_value}" is not in mmdet labels'
                            )
                        self.label_map[predicted_value] = ls_label

        # label map is still empty, not predicted_values found in Label tags,
        # try to build mapping automatically: map LS labels.lower() to mmdet_labels.lower() directly
        if not self.label_map:
            # try to find thin <Label value="Vehicle" predicted_values="airplane,car">
            for ls_label, _ in self.labels_attrs.items():
                try:
                    index = mmdet_labels_lower.index(ls_label.lower())
                except ValueError:
                    continue  # no label found in mmdet labels
                else:
                    self.label_map[mmdet_labels[index]] = ls_label

        print("MMDetection => Label Studio mapping of labels :\n", self.label_map)