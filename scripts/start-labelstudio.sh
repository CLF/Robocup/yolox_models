#!/bin/bash

LOG_FILE:=/tmp/label-studio.log

source /vol/clf/label-studio/use_volume.bash 
export LOCAL_FILES_SERVING_ENABLED=true
export LABEL_STUDIO_LOCAL_FILES_SERVING_ENABLED=true 
export LABEL_STUDIO_LOCAL_FILES_SERVING_ENABLED=true
export LABEL_STUDIO_LOCAL_FILES_DOCUMENT_ROOT=/home/robocup/datasets

mkdir /home/robocup/datasets/eindhoven

#label-studio start -b &> $LOG_FILE &
label-studio start -b -p 8081
