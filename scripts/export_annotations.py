#!/usr/bin/env python3
import os
import json
import funcy
import argparse
import numpy as np
from pathlib import Path
from datetime import datetime
from typing import Literal, Any
from sklearn.model_selection import train_test_split
from skmultilearn.model_selection import iterative_train_test_split

from label_studio_sdk import Client, Project
from label_studio_converter import Converter
from label_studio_converter.utils import get_polygon_area, get_polygon_bounding_box
from label_studio_tools.core.utils.io import get_local_path


parser = argparse.ArgumentParser(prog='ExportAnnotations',
                                 description='Export annotations from Label Studio without downloading the images. ' +
                                             'The annotation can be split into train and test annotation files if necessary.')

parser.add_argument('-k', '--api_key', type=str, default="a0db8b6f77df7f8ac7a524d79f77b07dd89425fe", help="Your Label Studio API key.")
parser.add_argument('-u', '--url', type=str, default="http://localhost:8081", help="The url to access Label Studio.")
parser.add_argument('-p', '--project_names', type=str, nargs='*', help="The names of the projects to export.")
parser.add_argument('-o', '--out_dir', type=str, default="./export", help="Where to store the exported annotations.")
parser.add_argument('-f', '--format', choices=['COCO'], default='COCO',
                    help="Format of the annotation file. Default is COCO. Currently, only COCO is supported.")
parser.add_argument('--train_test_split', required=False, type=float,
                    help="Percentage of splitting the data into train test annotations. Keep empty if a single annotation file is wanted.")
parser.add_argument('-l', '--local', action='store_true',
                    help="Use absolute path of the Files, only works on local cloud and if labelstudio runs on the same machine.")
parser.add_argument('-r', '--data_root', type=str, default="/home/robocup/datasets", help="Data root for local storage in LS")

DATA_ROOT="/home/robocup/datasets"

def _get_labels(project_config):
    labels = set()
    categories = list()
    category_name_to_id = dict()

    for name, info in project_config.items():
        labels |= set(info['labels'])
        attrs = info['labels_attrs']
        for label in attrs:
            if attrs[label].get('category'):
                categories.append(
                    {'id': attrs[label].get('category'), 'name': label}
                )
                category_name_to_id[label] = attrs[label].get('category')
    labels_to_add = set(labels) - set(list(category_name_to_id.keys()))
    labels_to_add = sorted(list(labels_to_add))
    idx = 0
    while idx in list(category_name_to_id.values()):
        idx += 1
    for label in labels_to_add:
        categories.append({'id': idx, 'name': label})
        category_name_to_id[label] = idx
        idx += 1
        while idx in list(category_name_to_id.values()):
            idx += 1
    return categories, category_name_to_id


def _convert_to_coco(tasks, project_config, include_empty_images=False, annotator_id=0, local_file=False):
    def add_image(images, width, height, image_id, image_path):
        images.append(
            {
                'width': width,
                'height': height,
                'id': image_id,
                'file_name': image_path,
            }
        )
        return images

    images, categories, annotations = [], [], []
    categories, category_name_to_id = _get_labels(project_config)
    print(f"using local_file: {local_file}")
    for task_idx, task in enumerate(tasks):
        image_path = task['data']['image'].replace('/data/local-files/?d=', f"{DATA_ROOT}/") if local_file else get_local_path(task['data']['image'], download_resources=True)
        image_id = len(images)
        width = None
        height = None

        anns = task['annotations']

        # skip tasks without annotations
        if len(anns) == 0:
            continue

        if include_empty_images:
            images = add_image(images, width, height, image_id, image_path)

        for result in anns[annotator_id]['result']:
            category_name = None
            for key in ['rectanglelabels', 'polygonlabels', 'labels']:
                if key in result['value'] and len(result['value'][key]) > 0:
                    category_name = result['value'][key][0]
                    break
            if category_name is None:
                print("Unknown label type or labels are empty")
                continue

            if not height or not width:
                if 'original_width' not in result or 'original_height' not in result:
                    print(f'original_width or original_height not found in {image_path}')
                    continue
                width, height = result['original_width'], result['original_height']
                images = add_image(images, width, height, image_id, image_path)

            if category_name not in category_name_to_id:
                category_id = len(categories)
                category_name_to_id[category_name] = category_id
                categories.append({'id': category_id, 'name': category_name})
            category_id = category_name_to_id[category_name]

            annotation_id = len(annotations)

            if 'rectanglelabels' in result['value'] or 'labels' in result['value']:
                xywh = Converter.rotated_rectangle(result['value'])
                if xywh is None:
                    continue

                x, y, w, h = xywh
                x = x * result["original_width"] / 100
                y = y * result["original_height"] / 100
                w = w * result["original_width"] / 100
                h = h * result["original_height"] / 100

                annotations.append(
                    {
                        'id': annotation_id,
                        'image_id': image_id,
                        'category_id': category_id,
                        'segmentation': [],
                        'bbox': [x, y, w, h],
                        'ignore': 0,
                        'iscrowd': 0,
                        'area': w * h,
                    }
                )
            elif "polygonlabels" in result['value']:
                points_abs = [
                    (x / 100 * width, y / 100 * height) for x, y in result['value']["points"]
                ]
                x, y = zip(*points_abs)

                annotations.append(
                    {
                        'id': annotation_id,
                        'image_id': image_id,
                        'category_id': category_id,
                        'segmentation': [
                            [coord for point in points_abs for coord in point]
                        ],
                        'bbox': get_polygon_bounding_box(x, y),
                        'ignore': 0,
                        'iscrowd': 0,
                        'area': get_polygon_area(x, y),
                    }
                )
            else:
                raise ValueError("Unknown label type")

    coco_anns = {
        'images': images,
        'categories': categories,
        'annotations': annotations,
        'license': '',
        'info': {
            'year': datetime.now().year,
            'version': '1.0',
            'description': '',
            'contributor': 'Label Studio',
            'url': '',
            'date_created': str(datetime.now()),
        }
    }
    return coco_anns


def export_annotations(client: Client,
                       projects: list[Project],
                       export_format: Literal['COCO'] = 'COCO',
                       store_local_image_path: bool = False) -> Any | None:
    """
    Returns annotations from a list of projects in the given format.
    Make sure project labeling configs are all the same.
    Currently only supports COCO format.
    :param client: Label-Studio Client class, connected with the Label-Studio server
    :param projects: The projects to export the annotations from.
    :param export_format: The export format to export the annotations to.
    :param store_local_image_path: Set to true to store local images path instead of url.
    :return: annotations in the export format
    """
    if client.check_connection()['status'] != 'UP':
        print('Connection to client failed. Please check your connection and try again.')
        return
    if len(projects) == 0:
        print('No projects specified.')
        return
    project_config = projects[0].parsed_label_config
    tasks = []
    for project in projects:
        tasks.extend(project.get_tasks())

    annotations = None
    if export_format == 'COCO':
        annotations = _convert_to_coco(tasks, project_config, local_file=store_local_image_path)
    else:
        print("Unsupported export format")
        pass

    return annotations


def split_annotations(annotations: dict, format: str, split: float, multi_class: bool = False) -> tuple[Any, Any]:
    """
    Split the annotation file into train and test files.
    :param annotations: the annotations stored as dict, str, etc. depending on the export format
    :param format: the export format
    :param split: train split percentage
    :param multi_class: If annotations are multi-class, set this to true
    :return:
    """
    if format == 'COCO':
        return train_test_split_coco(annotations, split, multi_class)
    else:
        pass


def train_test_split_coco(coco_anns: dict, split: float, multi_class: bool = False) -> tuple[dict, dict]:
    """
    Split COCO annotation dictionary into train and test dictionaries.
    Each class is at least once represented in each annotation file,
    except when only one sample of a category is present
    :param coco_anns: the coco annotations dictionary
    :param split: percentage of train annotations
    :param multi_class: Set to true for multi class labels
    :return: tuple of train and test dictionaries
    """
    def filter_annotations(annotations, images):
        image_ids = funcy.lmap(lambda i: int(i['id']), images)
        return funcy.lfilter(lambda a: int(a['image_id']) in image_ids, annotations)

    def filter_images(images, annotations):
        annotation_ids = funcy.lmap(lambda i: int(i['image_id']), annotations)
        return funcy.lfilter(lambda a: int(a['id']) in annotation_ids, images)

    info = coco_anns.get('info')
    license = coco_anns.get('license')
    images = coco_anns.get('images')
    annotations = coco_anns.get('annotations')
    categories = coco_anns.get('categories')

    if multi_class:
        annotation_categories = funcy.lmap(lambda a: int(a['category_id']), annotations)
        # remove classes that has only one sample, because it can't be split into the training and testing sets
        annotation_categories = funcy.lremove(lambda i: annotation_categories.count(i) <= 1, annotation_categories)
        annotations = funcy.lremove(lambda i: i['category_id'] not in annotation_categories, annotations)
        anns_train, _, anns_test, _ = iterative_train_test_split(np.array([annotations]).T,
                                                                      np.array([annotation_categories]).T,
                                                                      test_size=1 - split)
        images_train = filter_images(images, anns_train.reshape(-1))
        anns_train = anns_train.reshape(-1).tolist()
        images_test = filter_images(images, anns_test.reshape(-1))
        anns_test = anns_test.reshape(-1).tolist()
    else:
        images_train, images_test = train_test_split(images, train_size=split)
        anns_train = filter_annotations(annotations, images_train)
        anns_test = filter_annotations(annotations, images_test)

    train_anns = {
        'images': images_train,
        'categories': categories,
        'annotations': anns_train,
        'info': info,
        'license': license
    }
    test_anns = {
        'images': images_test,
        'categories': categories,
        'annotations': anns_test,
        'info': info,
        'license': license
    }

    return train_anns, test_anns


def save_anns(anns: Any, export_format: str, file_name: Path):
    """
    Save annotations.
    :param anns: The annotations. The annotation type must match the exported file format
                 (dictionary for COCO format, string for YOLO format, etc.)
    :param export_format: The export format (e.g. COCO)
    :param file_name: The file name.
    """
    os.makedirs(file_name.parent, exist_ok=True)
    if export_format == 'COCO':
        with open(str(file_name), 'w') as f:
            json.dump(anns, f, indent=2, sort_keys=True)
            print('Saved annotations in annotations.json')
    else:
        print("Format not supported. Not saving annotations.")
        pass


if __name__ == '__main__':
    parsed_args = parser.parse_args()
    client = Client(parsed_args.url, parsed_args.api_key)
    project_names = parsed_args.project_names # ['New Project #2', 'New Project #1']
    export_format = parsed_args.format
    split = parsed_args.train_test_split
    out_dir = Path(parsed_args.out_dir) / project_names[0]
    projects = client.get_projects()
    projects = [project for project in projects if project.params['title'] in project_names]
    local_image = parsed_args.local
    DATA_ROOT=parsed_args.data_root

    anns = export_annotations(client, projects, export_format, local_image)

    save_anns(anns, export_format, out_dir / "annotations/default_train.json")
    if split is not None:
        train_anns, test_anns = split_annotations(anns, export_format, split)
        save_anns(train_anns, export_format, out_dir / "annotations/default_train.json")
        save_anns(test_anns, export_format, out_dir / "annotations/default_val.json")
