
# Yolo Models

## Train new Dataset

### Create Annotation

0. Collect Images
1. Start label-studio
    ```
    scripts/start-labelstudio.sh
    ```
  1. Create new folder for dataset inside `LABEL_STUDIO_LOCAL_FILES_DOCUMENT_ROOT` (check start script)
  2. Register account (any mail + pwd)
  3. Create new Project with bounding box task
  4. Add local cloud storage, regex .*(jpg), enable every object as source file
  5. Add images to folder and sync the storage 
2. Optionally start yolox prediction backend, make sure to update the API key
    ```
    start-labelstudio-ml-yolox.sh
    ```
    And setup the model in the project 
3. Label Images

### Prepare training

0. Choose projects you want to use to retrain yolox. You can combine as many projects (1 to n) as you want.
1. Find your API key. Click at your user-icon at the top right corner of the label studio webpage. Then choose `Account and Settings`, your access-token should show up now. 
2. Export COCO files. 
    ```
    scripts/export.sh
    pip install -r scripts/requirements.txt
    scripts/export_annotations.py -l -k <API_KEY> -p <PROJECT_NAMES> --train_test_split 0.8
    ```

    Note: Exported annotations are stored in `~/yolox_models/export/<FIRST_PROJECT_NAME>`by default, where `<FIRST_PROJECT_NAME>` corresponds    to     the name of the first project listed at annotation export. This folder is needed in the next step as parameter (COCO_ROOT_FOLDER).

### Train YOLOX

0. To train yolox on coco dataset, take a look at start_training.py parameters. 
    ```
    start_training.py -n <MODEL_NAME> -r <COCO_ROOT_FOLDER>
    ```

    Please choose a meaningfull project name! (It can differ from the names of the projects used for training)

1. Train the model with `start_training.py` script
2. Move the final model (`best_ckpt.pth`) from `YOLOX_outputs/<MODEL_NAME>` to `models/<MODEL_NAME>`
3. Make sure the corresponding config yaml in `~/yolox_models/config` and the exp file in `~/yolox_models/config/exp` have been successfully generated.
4. Push the files mentioned in the two steps before. **Don't push any other files!**
  
