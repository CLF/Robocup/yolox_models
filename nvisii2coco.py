import json
import os
import glob
import shutil
import datetime
import pprint
import random

from yolox_models.datasets.gazebo_ycb_nvisii import ycb_classes

VAL_SET_SIZE = 2000
NVISII_DIR = '/home/jzilke/training/ycb_p1/'
COCO_DIR = '/home/jzilke/training/ycb_coco/'



today = str(datetime.date.today())
num_img = sum([len(glob.glob(folder + "*.png")) for folder in glob.glob(NVISII_DIR + "*/")])

val_ids = random.sample(range(1, num_img + 1), VAL_SET_SIZE)

img_path_train = COCO_DIR + "/train2017/"
if not os.path.exists(img_path_train):
    os.makedirs(img_path_train) 

img_path_val = COCO_DIR + "/val2017/"
if not os.path.exists(img_path_val):
    os.makedirs(img_path_val) 

obj_id_test = 1
img_id_test = 1

obj_id_val = 1
img_id_val = 1


categories = []
for id, cls in ycb_classes.get_id2obj().items():
    categories.append({
        "id": id,
        "name": cls,
        "supercategory": cls
    })


coco_json_test = {
    "info": {
        "year": "2023",
        "version": "1.0",
        "description": "Generated Dataset",
        "date_created": "2023"
        },
    "type": "instances",
    "licenses": [
        {
            "id": 1,
            "name": "MIT"
        }
    ],
    "categories": categories,
    "images": [],
    "annotations": []
    }

coco_json_val = {
    "info": {
        "year": "2023",
        "version": "1.0",
        "description": "Generated Dataset",
        "date_created": "2023"
        },
    "type": "instances",
    "licenses": [
        {
            "id": 1,
            "name": "MIT"
        }
    ],
    "categories": categories,
    "images": [],
    "annotations": []
    }

def get_image_dict(old_name, new_id: int, val=False):
    global obj_id_test, obj_id_val
    nvisii_dict = json.load(open(old_name[:-3] + "json"))
    image_dict = {
        "date_captured": today,
        "file_name": f"{new_id}.png",
        "id": new_id,
        "height": nvisii_dict["camera_data"]["height"],
        "width": nvisii_dict["camera_data"]["width"]
    }
    coco_obj_annotations = []
    for obj in nvisii_dict["objects"]:
        if obj["class"] in ycb_classes.get_classes():
            old_bbox = obj["bounding_box_minx_maxx_miny_maxy"]
            coco_obj = {
                "bbox": [old_bbox[0], old_bbox[2], old_bbox[1] - old_bbox[0], old_bbox[3] - old_bbox[2]],
                "area": (old_bbox[1] - old_bbox[0]) * (old_bbox[3] - old_bbox[2]),
                "iscrowd": 0,
                "image_id": new_id,
                "category_id": ycb_classes.get_obj2id()[obj["class"]],
                "id": obj_id_test if not val else obj_id_val
            }
            obj_id_test += 1 if not val else 0
            obj_id_val += 1 if val else 0
            coco_obj_annotations.append(coco_obj)
    return image_dict, coco_obj_annotations


def format(nvisii_path, coco_path):
    global img_id_test, img_id_val
    folders = glob.glob(nvisii_path + "*/")
    for folder in folders:
        imgs = glob.glob(folder + "*.png")
        for img in imgs:
            if img_id_test not in val_ids:
                shutil.copyfile(img, f"{img_path_train}{img_id_test}.png")
                image_d, coco_obj_annotations = get_image_dict(img, img_id_test)
                coco_json_test["images"].append(image_d)
                coco_json_test["annotations"].extend(coco_obj_annotations)
                img_id_test += 1
            else:
                shutil.copyfile(img, f"{img_path_val}{img_id_val}.png")
                image_dict, coco_obj_annotations = get_image_dict(img, img_id_val, True)
                coco_json_val["images"].append(image_dict)
                coco_json_val["annotations"].extend(coco_obj_annotations)
                val_ids.remove(img_id_test)
                img_id_val += 1



format(NVISII_DIR, COCO_DIR)

ann_path = COCO_DIR + "/annotations/"
if not os.path.exists(ann_path):
    os.makedirs(ann_path) 
with open(ann_path + "instances_train2017.json", 'w') as fp:
    json.dump(coco_json_test, fp)
with open(ann_path + "instances_val2017.json", 'w') as fp:
    json.dump(coco_json_val, fp)
