cmake_minimum_required(VERSION 2.8.12)
project(yolox_models)

find_package(catkin REQUIRED COMPONENTS
  rospy
)

catkin_python_setup()

catkin_package()

install(DIRECTORY launch config models 
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
